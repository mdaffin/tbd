# TO BE DIRE

https://septum.itch.io/to-be-dire

## Rusty Jam information

### Theme

_Illusion of Security_

### Links

- [itch.io](https://itch.io/jam/rusty-jam)
- [Discord](https://discord.gg/8dUQJFFmxG)

## Brainstorm

### Candidates for theme implementation

- Hacking - Penetrate a firewall
- Stealth - Infiltrate an enemy base
- **Survival - Venture out of your home** (selected)
- Shooter -  Break into an enemy base

### Design ideas

- 2D
- Tile-based
- Top-down perspective
- Pixel art

## Rust Crates

## Bevy (Engine)

### Links

- [GitHub](https://github.com/bevyengine/bevy/)
- [docs.rs](https://docs.rs/bevy/0.5.0/bevy/)
- [Learning Assets](https://bevyengine.org/assets/#learning)
- [Discord](https://discord.gg/bevy)
- [Cheatbook](https://bevy-cheatbook.github.io/)

## Tools

### Aseprite

Animated Sprite Editor & Pixel Art Tool

#### Links

- [Website](https://www.aseprite.org/)
- [GitHub](https://github.com/aseprite/aseprite/)
- [Documentation](https://www.aseprite.org/docs/)

### Tiled

Tiled is a free and open source, easy to use, and flexible level editor

#### Links

- [Website](https://www.mapeditor.org/)
- [GitHub](https://github.com/mapeditor/tiled)
- [Documentation](https://doc.mapeditor.org/en/stable/)

## Team

### Timezones

- SamiP: https://time.is/UTC+3
- Nous: https://time.is/UTC+1
- septum: https://time.is/UTC-5

use bevy::prelude::*;
use bevy_kira_audio::Audio;

use crate::{
    components::generic::{Direction, *},
    fear::Fear,
    inventory::{FoodInventory, InventoryBundle, WoodInventory},
    monsters::{BossMonster, Monster},
    utils::is_colliding,
};

pub struct PlayerPlugin;

pub struct Win(pub bool);

pub struct Player;
pub type WithAlivePlayer = (With<Player>, With<Alive>);

#[derive(Bundle)]
pub struct PlayerBundle {
    tag: Player,
    direction: Direction,
    health: Health,
    fear: Fear,
    hunger: Hunger,
    safe: Safe,
    alive: Alive,
    win: Win,
    #[bundle]
    inventory: InventoryBundle,
}

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system())
            .add_system(hunger.system())
            .add_system(eat_food_inventory.system())
            .add_system(movement.system())
            .add_system(see_no_danger.system())
            .add_system(death.system())
            .add_system(win.system());
    }
}

pub fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    let texture_handle = asset_server.load("images/character.png");
    let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(16.0, 16.0), 4, 1);
    let texture_atlas_handle = texture_atlases.add(texture_atlas);
    let transform = Transform::from_xyz(0.0, 16.0, 2.0);

    commands
        .spawn_bundle(SpriteSheetBundle {
            sprite: TextureAtlasSprite {
                index: 2,
                ..Default::default()
            },
            texture_atlas: texture_atlas_handle,
            transform,
            ..Default::default()
        })
        .insert_bundle(PlayerBundle::default());
}

pub fn hunger(
    time: Res<Time>,
    mut query: Query<(&mut Hunger, &mut Health, &Safe), WithAlivePlayer>,
) {
    if let Ok((mut hunger, mut health, safe)) = query.single_mut() {
        let is_safe = safe.0;
        let is_hungry = hunger.0.value() <= 0.01;

        hunger
            .0
            .sub(if is_safe { 0.25 } else { 0.5 } * time.delta_seconds());

        health.add(
            match (is_safe, is_hungry) {
                (true, true) => -0.5,
                (true, false) => 0.75,
                (false, true) => -1.0,
                (false, false) => 0.125,
            } * time.delta_seconds(),
        );
    }
}

pub fn eat_food_inventory(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    mut query: Query<(&Safe, &mut Hunger, &mut FoodInventory), WithAlivePlayer>,
) {
    if let Ok((safe, mut hunger, mut food)) = query.single_mut() {
        if safe.0 && hunger.0.value() <= 95.0 {
            if food.quantity.value() >= 5.0 {
                food.quantity.sub(5.0);
                hunger.0.add(5.0);
                audio.stop();
                audio.play(asset_server.load("audio/eating.mp3"));
            } else if food.quantity.value() > 0.0 {
                let food_remaining = food.quantity.value();
                food.quantity.sub(food_remaining);
                hunger.0.add(food_remaining);
                audio.stop();
                audio.play(asset_server.load("audio/eating.mp3"));
            }
        }
    }
}

pub fn movement(
    time: Res<Time>,
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&mut Transform, &mut Direction, &mut TextureAtlasSprite), WithAlivePlayer>,
) {
    if let Ok((mut transform, mut direction, mut sprite)) = query.single_mut() {
        let position = &mut transform.translation;
        let size = Vec2::new(16.0, 16.0);

        if keyboard_input.pressed(KeyCode::W) {
            direction.kind = DirectionKind::Up;
            position.y += 1.0 * time.delta_seconds() * 64.0;
            sprite.index = 0;
        }
        if keyboard_input.pressed(KeyCode::S) {
            direction.kind = DirectionKind::Down;
            position.y -= 1.0 * time.delta_seconds() * 64.0;
            sprite.index = 2;
        }
        if keyboard_input.pressed(KeyCode::A) {
            direction.kind = DirectionKind::Left;
            position.x -= 1.0 * time.delta_seconds() * 64.0;
            sprite.index = 1;
        }
        if keyboard_input.pressed(KeyCode::D) {
            direction.kind = DirectionKind::Right;
            position.x += 1.0 * time.delta_seconds() * 64.0;
            sprite.index = 3;
        }

        let bonfire_position = Vec3::new(0.0, 0.0, 1.0);
        let bonfire_size = Vec2::new(8.0, 8.0);

        let collision_detected = is_colliding(bonfire_position, bonfire_size, *position, size);

        if collision_detected {
            if keyboard_input.pressed(KeyCode::W) {
                position.y -= 1.0 * time.delta_seconds() * 64.0;
            }
            if keyboard_input.pressed(KeyCode::S) {
                position.y += 1.0 * time.delta_seconds() * 64.0;
            }
            if keyboard_input.pressed(KeyCode::A) {
                position.x += 1.0 * time.delta_seconds() * 64.0;
            }
            if keyboard_input.pressed(KeyCode::D) {
                position.x -= 1.0 * time.delta_seconds() * 64.0;
            }
        }
    }
}

pub fn death(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
    mut query: Query<(Entity, &mut Transform, &Health), WithAlivePlayer>,
) {
    if let Ok((entity, mut transform, health)) = query.single_mut() {
        if !health.is_alive() {
            info!("player died");
            transform.rotate(Quat::from_rotation_z(90.0));
            commands.entity(entity).remove::<Alive>();
            audio.stop();
            audio.play(asset_server.load("audio/player_death.mp3"));
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn win(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
    mut query_set: QuerySet<(
        Query<(&mut Win, Entity), WithAlivePlayer>,
        Query<&Health, With<BossMonster>>,
    )>,
) {
    let mut boss_monster_alive = false;

    if let Ok(health) = query_set.q1().single() {
        boss_monster_alive = health.0.value() >= 0.0;
    }

    if let Ok((mut win, player)) = query_set.q0_mut().single_mut() {
        if !boss_monster_alive {
            info!("player won");
            win.0 = true;
            commands.entity(player).remove::<Alive>();
            audio.stop();
            audio.play(asset_server.load("audio/boss_death.mp3"));
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn see_no_danger(
    mut query_set: QuerySet<(
        Query<&Safe, With<Player>>,
        Query<&mut Transform, With<Monster>>,
    )>,
) {
    let mut player_is_safe = false;

    if let Ok(safe) = query_set.q0_mut().single_mut() {
        player_is_safe = safe.0;
    }

    for mut transform in query_set.q1_mut().iter_mut() {
        if player_is_safe {
            transform.translation.z = -1.0;
        } else {
            transform.translation.z = 1.0;
        }
    }
}

impl Default for PlayerBundle {
    fn default() -> Self {
        PlayerBundle {
            tag: Player,
            direction: Direction {
                kind: DirectionKind::Down,
            },
            health: Health::new(0.0, 100.0, 100.0),
            fear: Fear(CappedValue::new(0.0, 100.0, 0.0)),
            hunger: Hunger(CappedValue::new(0.0, 100.0, 100.0)),
            safe: Safe(false),
            alive: Alive,
            win: Win(false),
            inventory: InventoryBundle {
                food: FoodInventory {
                    quantity: CappedValue::new(0.0, 999.0, 0.0),
                },
                wood: WoodInventory {
                    quantity: CappedValue::new(0.0, 999.0, 0.0),
                },
            },
        }
    }
}

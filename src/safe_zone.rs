use bevy::prelude::*;

use crate::bonfire::{Bonfire, BonfireWood};
use crate::components::generic::Safe;
use crate::player::Player;
use crate::utils::is_colliding;

pub struct SafeZonePlugin;
pub struct SafeZone;

impl Plugin for SafeZonePlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system())
            .add_system(player_is_safe.system())
            .add_system(sync_with_bonfire.system());
    }
}

pub fn setup(mut commands: Commands, mut materials: ResMut<Assets<ColorMaterial>>) {
    commands
        .spawn_bundle(SpriteBundle {
            material: materials.add(Color::rgba(0.9921, 0.6796, 0.2031, 0.25).into()),
            transform: Transform::from_xyz(0.0, 0.0, 2.0),
            sprite: Sprite::new(Vec2::new(120.0, 120.0)),
            ..Default::default()
        })
        .insert(SafeZone);
}

#[allow(clippy::type_complexity)]
pub fn player_is_safe(
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut query_set: QuerySet<(
        Query<(&Transform, &Sprite, &Handle<ColorMaterial>), With<SafeZone>>,
        Query<(&Transform, &mut Safe), With<Player>>,
    )>,
) {
    let mut safe_zone_position = Vec3::ZERO;
    let mut safe_zone_size = Vec2::ZERO;

    let mut player_position = Vec3::ZERO;
    let player_size = Vec2::new(16.0, 16.0);

    let mut collision_detected = false;

    if let Ok((transform, sprite, _)) = query_set.q0_mut().single_mut() {
        safe_zone_position += transform.translation;
        safe_zone_size += sprite.size;
    }

    if let Ok((transform, mut safe)) = query_set.q1_mut().single_mut() {
        player_position += transform.translation;

        collision_detected = is_colliding(
            safe_zone_position,
            safe_zone_size,
            player_position,
            player_size,
        );

        if collision_detected {
            safe.0 = true;
        } else {
            safe.0 = false;
        }
    }

    if let Ok((_, _, color)) = query_set.q0_mut().single_mut() {
        let mut material = materials.get_mut(color).unwrap();
        if collision_detected {
            material.color = Color::rgba(0.9921, 0.6796, 0.2031, 0.25);
        } else {
            material.color = Color::rgba(0.9921, 0.6796, 0.2031, 0.35);
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn sync_with_bonfire(
    mut query_set: QuerySet<(
        Query<&BonfireWood, With<Bonfire>>,
        Query<(&mut Transform, &mut Sprite), With<SafeZone>>,
    )>,
) {
    let mut wood_left: f32 = 0.0;

    if let Ok(wood) = query_set.q0().single() {
        wood_left = wood.quantity.value();
    }

    if let Ok((mut _transform, mut sprite)) = query_set.q1_mut().single_mut() {
        sprite.size.x = 120.0 * (wood_left / 100.0);
        sprite.size.y = 120.0 * (wood_left / 100.0);
    }
}

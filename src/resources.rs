use bevy::prelude::*;
use bevy_kira_audio::Audio;

use crate::{
    components::generic::{Direction, DirectionKind},
    inventory::{FoodInventory, WoodInventory},
    player::WithAlivePlayer,
    proc_gen::{bush_noise, hashc},
    utils::is_colliding,
};

pub struct ResourcePlugin;
pub struct WoodPlugin;
pub struct FoodPlugin;

impl Plugin for ResourcePlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system())
            .add_system(player_interaction.system());
    }
}

pub struct Edible {
    pub value: f32,
}

pub struct Burnable {
    pub value: f32,
}

pub struct EdibleExhausted(pub bool);
pub struct BurnableExhausted(pub bool);

#[derive(Bundle)]
pub struct ResourceBundle {
    edible: Edible,
    burnable: Burnable,
    edible_exhausted: EdibleExhausted,
    burnable_exhausted: BurnableExhausted,
    #[bundle]
    spritesheet: SpriteSheetBundle,
}

impl ResourceBundle {
    pub fn spawn_bush(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        texture_atlasses: &mut ResMut<Assets<TextureAtlas>>,
        x: f32,
        y: f32,
    ) {
        let texture_handle = asset_server.load("images/bush.png");
        let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(16.0, 16.0), 2, 1);
        let texture_atlas_handle = texture_atlasses.add(texture_atlas);

        commands.spawn_bundle(ResourceBundle {
            edible: Edible { value: 2.0 },
            burnable: Burnable { value: 2.0 },
            edible_exhausted: EdibleExhausted(false),
            burnable_exhausted: BurnableExhausted(false),
            spritesheet: SpriteSheetBundle {
                sprite: TextureAtlasSprite {
                    index: 0,
                    ..Default::default()
                },
                texture_atlas: texture_atlas_handle,
                transform: Transform::from_xyz(x, y, 1.0),
                ..Default::default()
            },
        });
    }
}

pub fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlasses: ResMut<Assets<TextureAtlas>>,
) {
    for y in -64..63 {
        for x in -64..63 {
            let noise = bush_noise(x as f32, y as f32);
            if (noise > 1.25 || noise < -1.25) && (hashc((x + (y << 16)) as u64) & 7) == 7 {
                ResourceBundle::spawn_bush(
                    &mut commands,
                    &asset_server,
                    &mut texture_atlasses,
                    16.0 * (x as f32),
                    16.0 * (y as f32),
                );
            }
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn player_interaction(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    keyboard_input: Res<Input<KeyCode>>,
    mut query_set: QuerySet<(
        Query<(
            &Transform,
            &mut Edible,
            &mut EdibleExhausted,
            &mut Burnable,
            &mut BurnableExhausted,
            &mut TextureAtlasSprite,
        )>,
        Query<
            (
                &Transform,
                &Direction,
                &mut FoodInventory,
                &mut WoodInventory,
            ),
            WithAlivePlayer,
        >,
    )>,
) {
    // TODO: move keyboard input to player and setup a component flag
    if keyboard_input.pressed(KeyCode::Space) {
        let (player_position, player_size) = match query_set.q1_mut().single_mut() {
            Err(_) => return,
            Ok((player_transform, direction, _, _)) => {
                let mut player_position = player_transform.translation;
                // TODO: move to player module
                // provide an aditional offset so that the interaction area
                // is always in front of the player character
                match direction.kind {
                    DirectionKind::Up => {
                        player_position.y += 4.0;
                    }
                    DirectionKind::Down => {
                        player_position.y -= 4.0;
                    }
                    DirectionKind::Left => {
                        player_position.x -= 4.0;
                    }
                    DirectionKind::Right => {
                        player_position.x += 4.0;
                    }
                    _ => (),
                }
                // TODO get size from sprite
                (player_position, Vec2::new(16.0, 16.0))
            }
        };

        let (collected_food, collected_wood) = query_set
            .q0_mut()
            .iter_mut()
            .filter(|(_, _, food_exhausted, _, wood_exhausted, _)| {
                !food_exhausted.0 || !wood_exhausted.0
            })
            .filter(|(resource_transform, _, _, _, _, _)| {
                // TODO get size from sprite
                let resource_size = Vec2::new(16.0, 16.0);
                is_colliding(
                    resource_transform.translation,
                    resource_size,
                    player_position,
                    player_size,
                )
            })
            .map(
                |(_, food, mut food_exhausted, wood, mut wood_exhausted, mut sprite)| {
                    audio.play(asset_server.load("audio/bushes.mp3"));
                    food_exhausted.0 = true;
                    wood_exhausted.0 = true;
                    sprite.index = 1;
                    (food.value, wood.value)
                },
            )
            .fold((0.0, 0.0), |(food_acc, wood_acc), (food, wood)| {
                (food_acc + food, wood_acc + wood)
            });

        if let Ok((_, _, mut food, mut wood)) = query_set.q1_mut().single_mut() {
            food.quantity.add(collected_food);
            wood.quantity.add(collected_wood);
        }
    }
}

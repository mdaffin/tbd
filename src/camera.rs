use bevy::prelude::*;

use crate::player::Player;

pub struct CameraPlugin;
pub struct MainCamera;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system())
            .add_system(follow_player.system());
    }
}

pub fn setup(mut commands: Commands) {
    let mut camera_bundle = OrthographicCameraBundle::new_2d();
    camera_bundle.orthographic_projection.scale *= 0.25;
    commands.spawn_bundle(camera_bundle).insert(MainCamera);
}

#[allow(clippy::type_complexity)]
pub fn follow_player(
    mut query_set: QuerySet<(
        Query<&Transform, With<Player>>,
        Query<&mut Transform, With<MainCamera>>,
    )>,
) {
    let player_translation = match query_set.q0().single() {
        Ok(transform) => transform.translation,
        _ => return,
    };
    if let Ok(mut camera_transform) = query_set.q1_mut().single_mut() {
        camera_transform.translation = player_translation;
    }
}

use std::ops::Deref;

pub struct Position {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone)]
pub enum DirectionKind {
    Up,
    Left,
    Down,
    Right,
    Idle,
}

pub struct Direction {
    pub kind: DirectionKind,
}

pub struct Health(pub CappedValue);
pub struct Hunger(pub CappedValue);

pub struct Safe(pub bool);

pub struct Alive;

pub struct CappedValue {
    min: f32,
    max: f32,
    value: f32,
}

impl Health {
    pub fn new(min: f32, max: f32, value: f32) -> Self {
        Self(CappedValue::new(min, max, value))
    }

    pub fn is_alive(&self) -> bool {
        self.0.value() >= 0.001
    }

    pub fn value(&self) -> f32 {
        self.0.value()
    }

    pub fn set(&mut self, value: f32) {
        self.0.set(value)
    }

    pub fn add(&mut self, value: f32) {
        self.0.add(value)
    }

    pub fn sub(&mut self, value: f32) {
        self.0.sub(value)
    }
}

impl CappedValue {
    pub fn new(min: f32, max: f32, value: f32) -> Self {
        assert!(min < max, "min value must be less then max value");
        let mut s = Self { min, max, value };
        s.set(value);
        s
    }

    pub fn value(&self) -> f32 {
        self.value
    }

    pub fn set(&mut self, value: f32) {
        self.value = f32::min(f32::max(value, self.min), self.max);
    }

    pub fn add(&mut self, value: f32) {
        self.set(self.value() + value);
    }

    pub fn sub(&mut self, value: f32) {
        self.set(self.value() - value);
    }
}

impl Deref for CappedValue {
    type Target = f32;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

impl DirectionKind {
    pub fn reverse(&self) -> DirectionKind {
        match self {
            DirectionKind::Up => DirectionKind::Down,
            DirectionKind::Down => DirectionKind::Up,
            DirectionKind::Left => DirectionKind::Right,
            DirectionKind::Right => DirectionKind::Left,
            DirectionKind::Idle => DirectionKind::Idle,
        }
    }
}

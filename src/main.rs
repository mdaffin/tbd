use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;
use bevy_kira_audio::{Audio, AudioChannel, AudioPlugin};

use tbd::conf::Config;
use tbd::*;

fn startup(mut commands: Commands, asset_server: Res<AssetServer>) {
    let mut camera_bundle = OrthographicCameraBundle::new_2d();

    camera_bundle.orthographic_projection.scale *= 0.25;

    commands.spawn_bundle(camera_bundle);

    let handle: Handle<TiledMap> = asset_server.load("data/map.tmx");

    let map_entity = commands.spawn().id();

    let transform = Transform::from_xyz(-1016.0, -1032.0, 0.0);

    commands.entity(map_entity).insert_bundle(TiledMapBundle {
        tiled_map: handle,
        map: Map::new(0u16, map_entity),
        transform,
        ..Default::default()
    });
}

fn main() {
    env_logger::Builder::from_default_env()
        .filter_level(log::LevelFilter::Info)
        .init();

    App::build()
        .add_plugin(Config)
        .add_plugins(DefaultPlugins)
        .add_plugin(TilemapPlugin)
        .add_plugin(TiledMapPlugin)
        .add_plugin(AudioPlugin)
        .add_startup_system(start_background_audio.system())
        .add_startup_system(startup.system())
        .add_plugin(camera::CameraPlugin)
        .add_plugin(player::PlayerPlugin)
        .add_plugin(ui::UiPlugin)
        .add_plugin(fear::FearPlugin)
        .add_plugin(monsters::MonsterPlugin)
        .add_plugin(resources::ResourcePlugin)
        .add_plugin(safe_zone::SafeZonePlugin)
        .add_plugin(bonfire::BonfirePlugin)
        .add_system(utils::set_texture_filters_to_nearest.system())
        .add_system(bevy::input::system::exit_on_esc_system.system())
        .run();
}

pub fn start_background_audio(asset_server: Res<AssetServer>, audio: Res<Audio>) {
    let background_channel = AudioChannel::new("background_channel".to_owned());
    audio.set_volume_in_channel(0.25, &background_channel);
    audio.play_looped_in_channel(
        asset_server.load("audio/overworld.mp3"),
        &background_channel,
    );

    audio.set_volume(0.75);
}

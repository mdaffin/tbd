use bevy::prelude::*;

use crate::{
    components::generic::{CappedValue, Safe},
    player::WithAlivePlayer,
};

pub struct FearPlugin;
pub struct Fear(pub CappedValue);

impl Plugin for FearPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system(player_fear.system());
    }
}

#[allow(clippy::type_complexity)]
pub fn player_fear(time: Res<Time>, mut query: Query<(&mut Fear, &Safe), WithAlivePlayer>) {
    if let Ok((mut fear, safe)) = query.single_mut() {
        fear.0
            .add(if safe.0 { -4.0 } else { 2.0 } * time.delta_seconds());
    }
}

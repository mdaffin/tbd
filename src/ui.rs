use bevy::{prelude::*, ui::Val::Px};

use crate::{
    components::generic::{Health, Hunger},
    fear::Fear,
    inventory::{FoodInventory, WoodInventory},
    player::Player,
};

pub struct UiPlugin;

pub struct HealthUi;
pub struct HungerUi;
pub struct FearUi;
pub struct FoodUi;
pub struct WoodUi;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system())
            .add_system(update_hunger.system())
            .add_system(update_health.system())
            .add_system(update_food.system())
            .add_system(update_fear.system())
            .add_system(update_wood.system());
    }
}

pub fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn_bundle(UiCameraBundle::default());
    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                justify_content: JustifyContent::FlexEnd,
                size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                ..Default::default()
            },
            material: materials.add(Color::NONE.into()),
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(NodeBundle {
                    style: Style {
                        flex_direction: FlexDirection::ColumnReverse,
                        padding: Rect::all(Val::Px(5.0)),
                        size: Size::new(Val::Px(420.0), Val::Px(142.0)),
                        ..Default::default()
                    },
                    material: materials.add(Color::rgb(0.15, 0.15, 0.15).into()),
                    ..Default::default()
                })
                .with_children(|parent| {
                    parent
                        .spawn_bundle(NodeBundle {
                            style: Style {
                                align_items: AlignItems::Center,
                                justify_content: JustifyContent::FlexStart,
                                margin: Rect::all(Val::Px(5.0)),
                                size: Size::new(Val::Px(400.0), Val::Px(20.0)),
                                ..Default::default()
                            },
                            material: materials.add(Color::NONE.into()),
                            ..Default::default()
                        })
                        .with_children(|parent| {
                            parent.spawn_bundle(TextBundle {
                                style: Style {
                                    size: Size::new(Val::Px(100.0), Val::Px(20.0)),
                                    ..Default::default()
                                },
                                text: Text::with_section(
                                    "HEALTH",
                                    TextStyle {
                                        font: asset_server.load("fonts/slkscr.ttf"),
                                        font_size: 14.0,
                                        color: Color::WHITE,
                                    },
                                    Default::default(),
                                ),
                                ..Default::default()
                            });

                            parent
                                .spawn_bundle(ui_bar_graph(
                                    materials.add(Color::rgb(0.9, 0.2, 0.1).into()),
                                ))
                                .insert(HealthUi);
                        });

                    parent
                        .spawn_bundle(NodeBundle {
                            style: Style {
                                align_items: AlignItems::Center,
                                justify_content: JustifyContent::FlexStart,
                                margin: Rect::all(Val::Px(5.0)),
                                size: Size::new(Val::Px(400.0), Val::Px(20.0)),
                                ..Default::default()
                            },
                            material: materials.add(Color::NONE.into()),
                            ..Default::default()
                        })
                        .with_children(|parent| {
                            parent.spawn_bundle(TextBundle {
                                style: Style {
                                    size: Size::new(Val::Px(100.0), Val::Px(20.0)),
                                    ..Default::default()
                                },
                                text: Text::with_section(
                                    "HUNGER",
                                    TextStyle {
                                        font: asset_server.load("fonts/slkscr.ttf"),
                                        font_size: 14.0,
                                        color: Color::WHITE,
                                    },
                                    Default::default(),
                                ),
                                ..Default::default()
                            });

                            parent
                                .spawn_bundle(ui_bar_graph(
                                    materials.add(Color::rgb(0.1, 0.9, 0.2).into()),
                                ))
                                .insert(HungerUi);
                        });

                    parent
                        .spawn_bundle(NodeBundle {
                            style: Style {
                                align_items: AlignItems::Center,
                                justify_content: JustifyContent::FlexStart,
                                margin: Rect::all(Val::Px(5.0)),
                                size: Size::new(Val::Px(400.0), Val::Px(20.0)),
                                ..Default::default()
                            },
                            material: materials.add(Color::NONE.into()),
                            ..Default::default()
                        })
                        .with_children(|parent| {
                            parent.spawn_bundle(TextBundle {
                                style: Style {
                                    size: Size::new(Val::Px(100.0), Val::Px(20.0)),
                                    ..Default::default()
                                },
                                text: Text::with_section(
                                    "FEAR",
                                    TextStyle {
                                        font: asset_server.load("fonts/slkscr.ttf"),
                                        font_size: 14.0,
                                        color: Color::WHITE,
                                    },
                                    Default::default(),
                                ),
                                ..Default::default()
                            });

                            parent
                                .spawn_bundle(ui_bar_graph(
                                    materials.add(Color::rgb(0.1, 0.2, 0.9).into()),
                                ))
                                .insert(FearUi);
                        });

                    // inventory
                    parent
                        .spawn_bundle(NodeBundle {
                            style: Style {
                                justify_content: JustifyContent::SpaceBetween,
                                margin: Rect {
                                    top: Val::Px(10.0),
                                    ..Default::default()
                                },
                                padding: Rect::all(Val::Px(5.0)),
                                size: Size::new(Val::Px(200.0), Val::Px(32.0)),
                                ..Default::default()
                            },
                            material: materials.add(Color::rgb(0.15, 0.15, 0.15).into()),
                            ..Default::default()
                        })
                        .with_children(|parent| {
                            // food
                            parent
                                .spawn_bundle(NodeBundle {
                                    style: Style {
                                        align_items: AlignItems::Center,
                                        size: Size::new(Val::Px(90.0), Val::Px(32.0)),
                                        ..Default::default()
                                    },
                                    material: materials.add(Color::rgb(0.15, 0.15, 0.15).into()),
                                    ..Default::default()
                                })
                                .with_children(|parent| {
                                    parent.spawn_bundle(NodeBundle {
                                        style: Style {
                                            size: Size::new(Val::Px(32.0), Val::Px(32.0)),
                                            ..Default::default()
                                        },
                                        material: materials
                                            .add(asset_server.load("images/apple.png").into()),
                                        ..Default::default()
                                    });

                                    parent
                                        .spawn_bundle(TextBundle {
                                            text: Text::with_section(
                                                "0",
                                                TextStyle {
                                                    font: asset_server.load("fonts/slkscr.ttf"),
                                                    font_size: 24.0,
                                                    color: Color::WHITE,
                                                },
                                                Default::default(),
                                            ),
                                            ..Default::default()
                                        })
                                        .insert(FoodUi);
                                });

                            // wood
                            parent
                                .spawn_bundle(NodeBundle {
                                    style: Style {
                                        align_items: AlignItems::Center,
                                        size: Size::new(Val::Px(90.0), Val::Px(32.0)),
                                        ..Default::default()
                                    },
                                    material: materials.add(Color::rgb(0.15, 0.15, 0.15).into()),
                                    ..Default::default()
                                })
                                .with_children(|parent| {
                                    parent.spawn_bundle(NodeBundle {
                                        style: Style {
                                            size: Size::new(Val::Px(32.0), Val::Px(32.0)),
                                            ..Default::default()
                                        },
                                        material: materials
                                            .add(asset_server.load("images/wood.png").into()),
                                        ..Default::default()
                                    });

                                    parent
                                        .spawn_bundle(TextBundle {
                                            text: Text::with_section(
                                                "0",
                                                TextStyle {
                                                    font: asset_server.load("fonts/slkscr.ttf"),
                                                    font_size: 24.0,
                                                    color: Color::WHITE,
                                                },
                                                Default::default(),
                                            ),
                                            ..Default::default()
                                        })
                                        .insert(WoodUi);
                                });
                        });
                });
        });
}

pub fn ui_bar_graph(material: Handle<ColorMaterial>) -> NodeBundle {
    NodeBundle {
        style: Style {
            size: Size::new(Val::Px(300.0), Val::Px(20.0)),
            ..Default::default()
        },
        material,
        ..Default::default()
    }
}

#[allow(clippy::type_complexity)]
pub fn update_hunger(
    mut query_set: QuerySet<(
        Query<&Hunger, With<Player>>,
        Query<&mut Style, With<HungerUi>>,
    )>,
) {
    let mut current_hunger: f32 = 0.0;

    if let Ok(hunger) = query_set.q0_mut().single_mut() {
        current_hunger = hunger.0.value();
    }

    if let Ok(mut style) = query_set.q1_mut().single_mut() {
        style.size.width = Px(current_hunger * 3.0);
    }
}

#[allow(clippy::type_complexity)]
pub fn update_health(
    mut query_set: QuerySet<(
        Query<&Health, With<Player>>,
        Query<&mut Style, With<HealthUi>>,
    )>,
) {
    let mut current_health: f32 = 0.0;

    if let Ok(health) = query_set.q0_mut().single_mut() {
        current_health = health.value();
    }

    if let Ok(mut style) = query_set.q1_mut().single_mut() {
        style.size.width = Px(current_health * 3.0);
    }
}

#[allow(clippy::type_complexity)]
pub fn update_fear(
    mut query_set: QuerySet<(Query<&Fear, With<Player>>, Query<&mut Style, With<FearUi>>)>,
) {
    let mut current_fear: f32 = 0.0;

    if let Ok(fear) = query_set.q0_mut().single_mut() {
        current_fear = fear.0.value();
    }

    if let Ok(mut style) = query_set.q1_mut().single_mut() {
        style.size.width = Px(current_fear * 3.0);
    }
}

#[allow(clippy::type_complexity)]
pub fn update_food(
    mut query_set: QuerySet<(
        Query<&FoodInventory, With<Player>>,
        Query<&mut Text, With<FoodUi>>,
    )>,
) {
    let mut current_food: f32 = 0.0;

    if let Ok(food) = query_set.q0_mut().single_mut() {
        current_food = food.quantity.value();
    }

    if let Ok(mut text) = query_set.q1_mut().single_mut() {
        text.sections[0].value = format!("{}", current_food.trunc());
    }
}

#[allow(clippy::type_complexity)]
pub fn update_wood(
    mut query_set: QuerySet<(
        Query<&WoodInventory, With<Player>>,
        Query<&mut Text, With<WoodUi>>,
    )>,
) {
    let mut current_wood: f32 = 0.0;

    if let Ok(wood) = query_set.q0_mut().single_mut() {
        current_wood = wood.quantity.value();
    }

    if let Ok(mut text) = query_set.q1_mut().single_mut() {
        text.sections[0].value = format!("{}", current_wood.trunc());
    }
}

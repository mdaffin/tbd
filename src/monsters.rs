use bevy_kira_audio::Audio;
use rand::prelude::*;
use std::time::Duration;

use bevy::{
    prelude::*,
    sprite::collide_aabb::{collide, Collision},
};

use crate::{
    components::generic::{CappedValue, Direction, DirectionKind, Health},
    fear::Fear,
    player::WithAlivePlayer,
    proc_gen::{bush_noise, hashc},
    safe_zone::SafeZone,
    utils::is_colliding,
};

pub struct MonsterPlugin;

pub struct MovementTimer(Timer);

pub struct Berserk(pub bool);

pub struct SmellingFear(pub bool);

pub struct Monster;

pub struct BossMonster;

pub struct RunAway(pub bool);

pub struct RunAwayDirections(pub Vec3);

impl Plugin for MonsterPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system())
            .add_system(movement.system())
            .add_system(boss_movement.system())
            .add_system(detect_player.system())
            .add_system(boss_detect_player.system())
            .add_system(hunt_player.system())
            .add_system(boss_hunt_player.system())
            .add_system(death_by_bonfire.system())
            .add_system(damage_by_bonfire.system())
            .add_system(smell_fear.system())
            .add_system(boss_smell_fear.system())
            .add_system(boss_run_away.system());
    }
}

#[derive(Bundle)]
pub struct MonsterBundle {
    direction: Direction,
    berserk: Berserk,
    smelling_fear: SmellingFear,
    #[bundle]
    spritesheet: SpriteSheetBundle,
}

#[derive(Bundle)]
pub struct BossMonsterBundle {
    health: Health,
    direction: Direction,
    berserk: Berserk,
    smelling_fear: SmellingFear,
    run_away: RunAway,
    run_away_directions: RunAwayDirections,
    #[bundle]
    spritesheet: SpriteSheetBundle,
}

impl MonsterBundle {
    pub fn spawn_monster(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        texture_atlasses: &mut ResMut<Assets<TextureAtlas>>,
        x: f32,
        y: f32,
    ) {
        let texture_handle = asset_server.load("images/monster.png");
        let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(16.0, 16.0), 4, 1);
        let texture_atlas_handle = texture_atlasses.add(texture_atlas);

        let mut rng = thread_rng();
        let random_value = rng.gen_range(1..=5);

        let (kind, index) = match random_value {
            1 => (DirectionKind::Up, 0),
            2 => (DirectionKind::Down, 2),
            3 => (DirectionKind::Left, 1),
            4 => (DirectionKind::Right, 3),
            _ => (DirectionKind::Idle, 2),
        };

        commands
            .spawn_bundle(MonsterBundle {
                direction: Direction { kind },
                berserk: Berserk(false),
                smelling_fear: SmellingFear(false),
                spritesheet: SpriteSheetBundle {
                    visible: Visible {
                        is_visible: true,
                        is_transparent: true,
                    },
                    sprite: TextureAtlasSprite {
                        index,
                        ..Default::default()
                    },
                    texture_atlas: texture_atlas_handle,
                    transform: Transform::from_xyz(x, y, 1.0),
                    ..Default::default()
                },
            })
            .insert(MovementTimer(Timer::from_seconds(2.0, true)))
            .insert(Monster);
    }
}

impl BossMonsterBundle {
    pub fn spawn_boss_monster(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        texture_atlasses: &mut ResMut<Assets<TextureAtlas>>,
        x: f32,
        y: f32,
    ) {
        let texture_handle = asset_server.load("images/boss.png");
        let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(128.0, 72.0), 4, 1);
        let texture_atlas_handle = texture_atlasses.add(texture_atlas);

        let mut rng = thread_rng();
        let random_value = rng.gen_range(1..=5);

        let (kind, index) = match random_value {
            1 => (DirectionKind::Up, 0),
            2 => (DirectionKind::Down, 2),
            3 => (DirectionKind::Left, 1),
            4 => (DirectionKind::Right, 3),
            _ => (DirectionKind::Idle, 2),
        };

        commands
            .spawn_bundle(BossMonsterBundle {
                health: Health(CappedValue::new(0.0, 300.0, 300.0)),
                direction: Direction { kind },
                berserk: Berserk(false),
                run_away: RunAway(false),
                run_away_directions: RunAwayDirections(Vec3::new(x, y, 2.0)),
                smelling_fear: SmellingFear(false),
                spritesheet: SpriteSheetBundle {
                    sprite: TextureAtlasSprite {
                        index,
                        ..Default::default()
                    },
                    texture_atlas: texture_atlas_handle,
                    transform: Transform::from_xyz(x, y, 2.0),
                    ..Default::default()
                },
            })
            .insert(MovementTimer(Timer::from_seconds(2.0, true)))
            .insert(BossMonster);
    }
}

pub fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlasses: ResMut<Assets<TextureAtlas>>,
) {
    let mut rng = thread_rng();
    let x = if rng.gen() { 1.0 } else { -1.0 } * 1024.0;
    let y = if rng.gen() { 1.0 } else { -1.0 } * 1024.0;

    BossMonsterBundle::spawn_boss_monster(
        &mut commands,
        &asset_server,
        &mut texture_atlasses,
        x,
        y,
    );

    for y in -64..63 {
        for x in -64..63 {
            let noise = bush_noise(x as f32, y as f32);
            if (noise > 1.25 || noise < -1.25) && (hashc((x + (y << 16)) as u64) & 7) == 7 {
                MonsterBundle::spawn_monster(
                    &mut commands,
                    &asset_server,
                    &mut texture_atlasses,
                    16.0 * (x as f32),
                    16.0 * (y as f32),
                );
            }
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn movement(
    time: Res<Time>,
    mut query: Query<
        (
            &mut Transform,
            &mut Direction,
            &mut TextureAtlasSprite,
            &mut MovementTimer,
            &Berserk,
            &SmellingFear,
        ),
        With<Monster>,
    >,
) {
    for (mut transform, mut direction, mut sprite, mut timer, berserk, smelling_fear) in
        query.iter_mut()
    {
        if !berserk.0 && !smelling_fear.0 {
            let mut direction_kind = Some(direction.kind.clone());
            let position = &mut transform.translation;

            if timer.0.finished() {
                let mut rng = thread_rng();
                let random_value = rng.gen_range(1..=12);

                direction_kind = match random_value {
                    1 => Some(DirectionKind::Up),
                    2 => Some(DirectionKind::Down),
                    3 => Some(DirectionKind::Left),
                    4 => Some(DirectionKind::Right),
                    5 => Some(direction.kind.reverse()),
                    6 => Some(direction.kind.clone()),
                    _ => Some(DirectionKind::Idle),
                };
            }

            match direction_kind {
                Some(DirectionKind::Up) => {
                    direction.kind = DirectionKind::Up;
                    position.y += 1.0 * time.delta_seconds() * 16.0;
                    sprite.index = 0;
                }
                Some(DirectionKind::Down) => {
                    direction.kind = DirectionKind::Down;
                    position.y -= 1.0 * time.delta_seconds() * 16.0;
                    sprite.index = 2;
                }
                Some(DirectionKind::Left) => {
                    direction.kind = DirectionKind::Left;
                    position.x -= 1.0 * time.delta_seconds() * 16.0;
                    sprite.index = 1;
                }
                Some(DirectionKind::Right) => {
                    direction.kind = DirectionKind::Right;
                    position.x += 1.0 * time.delta_seconds() * 16.0;
                    sprite.index = 3;
                }
                _ => (),
            }

            timer.0.tick(Duration::from_secs_f32(time.delta_seconds()));
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn boss_movement(
    time: Res<Time>,
    mut query: Query<
        (
            &mut Transform,
            &mut Direction,
            &mut TextureAtlasSprite,
            &mut MovementTimer,
            &Berserk,
            &SmellingFear,
            &RunAway,
        ),
        With<BossMonster>,
    >,
) {
    for (mut transform, mut direction, mut sprite, mut timer, berserk, smelling_fear, run_away) in
        query.iter_mut()
    {
        if !run_away.0 && !berserk.0 && !smelling_fear.0 {
            let mut direction_kind = Some(direction.kind.clone());
            let position = &mut transform.translation;

            if timer.0.finished() {
                let mut rng = thread_rng();
                let random_value = rng.gen_range(1..=12);

                direction_kind = match random_value {
                    1 => Some(DirectionKind::Up),
                    2 => Some(DirectionKind::Down),
                    3 => Some(DirectionKind::Left),
                    4 => Some(DirectionKind::Right),
                    5 => Some(direction.kind.reverse()),
                    6 => Some(direction.kind.clone()),
                    _ => Some(DirectionKind::Idle),
                };
            }

            match direction_kind {
                Some(DirectionKind::Up) => {
                    direction.kind = DirectionKind::Up;
                    position.y += 1.0 * time.delta_seconds() * 8.0;
                    sprite.index = 0;
                }
                Some(DirectionKind::Down) => {
                    direction.kind = DirectionKind::Down;
                    position.y -= 1.0 * time.delta_seconds() * 8.0;
                    sprite.index = 2;
                }
                Some(DirectionKind::Left) => {
                    direction.kind = DirectionKind::Left;
                    position.x -= 1.0 * time.delta_seconds() * 8.0;
                    sprite.index = 1;
                }
                Some(DirectionKind::Right) => {
                    direction.kind = DirectionKind::Right;
                    position.x += 1.0 * time.delta_seconds() * 8.0;
                    sprite.index = 3;
                }
                _ => (),
            }

            timer.0.tick(Duration::from_secs_f32(time.delta_seconds()));
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn boss_detect_player(
    mut query_set: QuerySet<(
        Query<&Transform, WithAlivePlayer>,
        Query<
            (
                &Transform,
                &mut Berserk,
                &mut Direction,
                &mut TextureAtlasSprite,
            ),
            With<BossMonster>,
        >,
    )>,
) {
    let mut player_position = Vec3::ZERO;
    let player_size = Vec2::new(16.0, 16.0);

    let monster_size = Vec2::new(256.0, 256.0);

    if let Ok(transform) = query_set.q0_mut().single_mut() {
        player_position += transform.translation;
    }

    for (transform, mut berserk, mut direction, mut sprite) in query_set.q1_mut().iter_mut() {
        let monster_position = transform.translation;

        let collision_side = collide(monster_position, monster_size, player_position, player_size);

        let collision_detected =
            is_colliding(monster_position, monster_size, player_position, player_size);

        if collision_detected {
            berserk.0 = true;

            if let Some(side) = collision_side {
                match side {
                    Collision::Top => {
                        direction.kind = DirectionKind::Down;
                        sprite.index = 2;
                    }
                    Collision::Bottom => {
                        direction.kind = DirectionKind::Up;
                        sprite.index = 0;
                    }
                    Collision::Left => {
                        direction.kind = DirectionKind::Right;
                        sprite.index = 3;
                    }
                    Collision::Right => {
                        direction.kind = DirectionKind::Left;
                        sprite.index = 1;
                    }
                }
            }
        } else {
            berserk.0 = false;
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn detect_player(
    mut query_set: QuerySet<(
        Query<&Transform, WithAlivePlayer>,
        Query<
            (
                &Transform,
                &mut Berserk,
                &mut Direction,
                &mut TextureAtlasSprite,
            ),
            With<Monster>,
        >,
    )>,
) {
    let mut player_position = Vec3::ZERO;
    let player_size = Vec2::new(16.0, 16.0);

    let monster_size = Vec2::new(80.0, 80.0);

    if let Ok(transform) = query_set.q0_mut().single_mut() {
        player_position += transform.translation;
    }

    for (transform, mut berserk, mut direction, mut sprite) in query_set.q1_mut().iter_mut() {
        let monster_position = transform.translation;

        let collision_side = collide(monster_position, monster_size, player_position, player_size);

        let collision_detected =
            is_colliding(monster_position, monster_size, player_position, player_size);

        if collision_detected {
            berserk.0 = true;

            if let Some(side) = collision_side {
                match side {
                    Collision::Top => {
                        direction.kind = DirectionKind::Down;
                        sprite.index = 2;
                    }
                    Collision::Bottom => {
                        direction.kind = DirectionKind::Up;
                        sprite.index = 0;
                    }
                    Collision::Left => {
                        direction.kind = DirectionKind::Right;
                        sprite.index = 3;
                    }
                    Collision::Right => {
                        direction.kind = DirectionKind::Left;
                        sprite.index = 1;
                    }
                }
            }
        } else {
            berserk.0 = false;
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn hunt_player(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    time: Res<Time>,
    mut query_set: QuerySet<(
        Query<(&Transform, &mut Fear, &mut Health), WithAlivePlayer>,
        Query<(&mut Transform, &Berserk), With<Monster>>,
    )>,
) {
    let mut player_position = Vec3::ZERO;
    let player_size = Vec2::new(16.0, 16.0);

    let monster_size = Vec2::new(16.0, 16.0);

    let mut damage: f32 = 0.0;
    let mut fear: f32 = 0.0;

    if let Ok((transform, _, _)) = query_set.q0_mut().single_mut() {
        player_position += transform.translation;
    }

    for (mut transform, berserk) in query_set.q1_mut().iter_mut() {
        if berserk.0 {
            let monster_position = transform.translation;

            fear += 0.3;

            let collision_detected =
                is_colliding(monster_position, monster_size, player_position, player_size);

            if collision_detected {
                damage += 2.0;
                fear += 2.0;
                audio.stop();
                audio.play(asset_server.load("audio/monster.mp3"));
            } else {
                let direction =
                    Vec2::normalize(player_position.truncate() - monster_position.truncate());

                transform.translation.x += direction.x * time.delta_seconds() * 48.0;
                transform.translation.y += direction.y * time.delta_seconds() * 48.0;
            }
        }
    }

    if let Ok((_, mut player_fear, mut health)) = query_set.q0_mut().single_mut() {
        health.0.sub(damage);
        player_fear.0.add(fear);
    }
}

#[allow(clippy::type_complexity)]
pub fn boss_hunt_player(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    time: Res<Time>,
    mut query_set: QuerySet<(
        Query<(&Transform, &mut Fear, &mut Health), WithAlivePlayer>,
        Query<(&mut Transform, &Berserk, &RunAway), With<BossMonster>>,
    )>,
) {
    let mut player_position = Vec3::ZERO;
    let player_size = Vec2::new(16.0, 16.0);

    let monster_size = Vec2::new(128.0, 72.0);

    let mut damage: f32 = 0.0;
    let mut fear: f32 = 0.0;

    if let Ok((transform, _, _)) = query_set.q0_mut().single_mut() {
        player_position += transform.translation;
    }

    for (mut transform, berserk, run_away) in query_set.q1_mut().iter_mut() {
        if !run_away.0 && berserk.0 {
            let monster_position = transform.translation;

            fear += 0.4;

            let collision_detected =
                is_colliding(monster_position, monster_size, player_position, player_size);

            if collision_detected {
                damage += 10.0;
                fear += 10.0;
                audio.stop();
                audio.play(asset_server.load("audio/boss.mp3"));
            } else {
                let direction =
                    Vec2::normalize(player_position.truncate() - monster_position.truncate());

                transform.translation.x += direction.x * time.delta_seconds() * 56.0;
                transform.translation.y += direction.y * time.delta_seconds() * 56.0;
            }
        }
    }

    if let Ok((_, mut player_fear, mut health)) = query_set.q0_mut().single_mut() {
        health.0.sub(damage);
        player_fear.0.add(fear);
    }
}

#[allow(clippy::type_complexity)]
pub fn death_by_bonfire(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
    mut query_set: QuerySet<(
        Query<(&Transform, &Sprite), With<SafeZone>>,
        Query<(&Transform, Entity), With<Monster>>,
    )>,
) {
    let mut safe_zone_position = Vec3::ZERO;
    let mut safe_zone_size = Vec2::ZERO;

    let monster_size = Vec2::new(16.0, 16.0);

    if let Ok((transform, sprite)) = query_set.q0_mut().single_mut() {
        safe_zone_position += transform.translation;
        safe_zone_size += sprite.size;
    }

    for (transform, monster) in query_set.q1().iter() {
        let monster_position = transform.translation;

        let collision_detected = is_colliding(
            safe_zone_position,
            safe_zone_size,
            monster_position,
            monster_size,
        );

        if collision_detected {
            commands.entity(monster).despawn();
            audio.stop();
            audio.play(asset_server.load("audio/monster_death.mp3"));
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn damage_by_bonfire(
    mut commands: Commands,
    mut query_set: QuerySet<(
        Query<(&Transform, &Sprite), With<SafeZone>>,
        Query<(&Transform, &mut Health, &mut RunAway, Entity), With<BossMonster>>,
    )>,
) {
    let mut safe_zone_position = Vec3::ZERO;
    let mut safe_zone_size = Vec2::ZERO;

    let monster_size = Vec2::new(128.0, 72.0);

    if let Ok((transform, sprite)) = query_set.q0_mut().single_mut() {
        safe_zone_position += transform.translation;
        safe_zone_size += sprite.size;
    }

    for (transform, mut health, mut run_away, boss_monster) in query_set.q1_mut().iter_mut() {
        if !run_away.0 {
            let monster_position = transform.translation;

            let collision_detected = is_colliding(
                safe_zone_position,
                safe_zone_size,
                monster_position,
                monster_size,
            );

            if collision_detected {
                health.0.sub(100.0);

                if health.0.value() <= 0.0 {
                    commands.entity(boss_monster).despawn();
                } else {
                    run_away.0 = true;
                }
            }
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn smell_fear(
    time: Res<Time>,
    mut query_set: QuerySet<(
        Query<(&Transform, &Fear, &Direction, &TextureAtlasSprite), WithAlivePlayer>,
        Query<
            (
                &mut Transform,
                &mut SmellingFear,
                &mut Direction,
                &mut TextureAtlasSprite,
                &Berserk,
            ),
            With<Monster>,
        >,
    )>,
) {
    let mut player_position = Vec3::ZERO;
    let mut fear_level: f32 = 0.0;
    let mut sprite_index: u32 = 0;
    let mut direction_kind = DirectionKind::Idle;

    if let Ok((transform, fear, direction, sprite)) = query_set.q0_mut().single_mut() {
        player_position += transform.translation;
        fear_level += fear.0.value();
        direction_kind = direction.kind.clone();
        sprite_index = sprite.index;
    }

    for (mut transform, mut smelling_fear, mut monster_direction, mut sprite, berserk) in
        query_set.q1_mut().iter_mut()
    {
        if !berserk.0 {
            if fear_level >= 100.0 {
                smelling_fear.0 = true;

                let monster_position = transform.translation;

                let direction =
                    Vec2::normalize(player_position.truncate() - monster_position.truncate());

                transform.translation.x += direction.x * time.delta_seconds() * 32.0;
                transform.translation.y += direction.y * time.delta_seconds() * 32.0;

                monster_direction.kind = direction_kind.clone();
                sprite.index = sprite_index;
            } else {
                smelling_fear.0 = false;
            }
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn boss_smell_fear(
    time: Res<Time>,
    mut query_set: QuerySet<(
        Query<(&Transform, &Fear, &Direction, &TextureAtlasSprite), WithAlivePlayer>,
        Query<
            (
                &mut Transform,
                &mut SmellingFear,
                &mut Direction,
                &mut TextureAtlasSprite,
                &Berserk,
                &RunAway,
            ),
            With<BossMonster>,
        >,
    )>,
) {
    let mut player_position = Vec3::ZERO;
    let mut fear_level: f32 = 0.0;
    let mut sprite_index: u32 = 0;
    let mut direction_kind = DirectionKind::Idle;

    if let Ok((transform, fear, direction, sprite)) = query_set.q0_mut().single_mut() {
        player_position += transform.translation;
        fear_level += fear.0.value();
        direction_kind = direction.kind.clone();
        sprite_index = sprite.index;
    }

    for (mut transform, mut smelling_fear, mut monster_direction, mut sprite, berserk, run_away) in
        query_set.q1_mut().iter_mut()
    {
        if !run_away.0 && !berserk.0 {
            if fear_level >= 100.0 {
                smelling_fear.0 = true;

                let monster_position = transform.translation;

                let direction =
                    Vec2::normalize(player_position.truncate() - monster_position.truncate());

                transform.translation.x += direction.x * time.delta_seconds() * 16.0;
                transform.translation.y += direction.y * time.delta_seconds() * 16.0;

                monster_direction.kind = direction_kind.clone();
                sprite.index = sprite_index;
            } else {
                smelling_fear.0 = false;
            }
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn boss_run_away(
    time: Res<Time>,
    mut query: Query<
        (
            &mut Transform,
            &mut RunAway,
            &mut RunAwayDirections,
            &mut TextureAtlasSprite,
        ),
        With<BossMonster>,
    >,
) {
    for (mut transform, mut run_away, mut directions, mut sprite) in query.iter_mut() {
        if run_away.0 {
            let distance =
                Vec2::distance(transform.translation.truncate(), directions.0.truncate());
            let direction =
                Vec2::normalize(directions.0.truncate() - transform.translation.truncate());

            transform.translation.x += direction.x * time.delta_seconds() * 128.0;
            transform.translation.y += direction.y * time.delta_seconds() * 128.0;

            if direction.y > 0.0 {
                sprite.index = 0;
            } else {
                sprite.index = 2;
            }

            if Vec2::distance(directions.0.truncate(), transform.translation.truncate()) >= distance
            {
                let mut rng = thread_rng();
                let x = if rng.gen() { 1.0 } else { -1.0 } * 1024.0;
                let y = if rng.gen() { 1.0 } else { -1.0 } * 1024.0;

                run_away.0 = false;
                directions.0.x = x;
                directions.0.y = y;
            }
        }
    }
}

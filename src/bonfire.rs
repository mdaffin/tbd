use bevy::prelude::*;
use bevy_kira_audio::Audio;

use crate::{
    components::generic::{CappedValue, Direction, DirectionKind},
    inventory::WoodInventory,
    player::WithAlivePlayer,
    utils::is_colliding,
};

pub struct BonfirePlugin;

impl Plugin for BonfirePlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system())
            .add_system(burn.system())
            .add_system(player_interaction.system());
    }
}

pub struct Bonfire;

pub struct BonfireWood {
    pub quantity: CappedValue,
}

#[derive(Bundle)]
pub struct BonfireBundle {
    wood: BonfireWood,
    #[bundle]
    spritesheet: SpriteSheetBundle,
}

pub fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlasses: ResMut<Assets<TextureAtlas>>,
) {
    let texture_handle = asset_server.load("images/bonfire.png");
    let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(16.0, 16.0), 4, 1);
    let texture_atlas_handle = texture_atlasses.add(texture_atlas);

    commands
        .spawn_bundle(BonfireBundle {
            wood: BonfireWood {
                quantity: CappedValue::new(0.0, 100.0, 100.0),
            },
            spritesheet: SpriteSheetBundle {
                sprite: TextureAtlasSprite {
                    index: 3,
                    ..Default::default()
                },
                texture_atlas: texture_atlas_handle,
                transform: Transform::from_xyz(0.0, 0.0, 1.0),
                ..Default::default()
            },
        })
        .insert(Bonfire);
}

#[allow(clippy::type_complexity)]
pub fn burn(
    time: Res<Time>,
    mut query: Query<(&mut BonfireWood, &mut TextureAtlasSprite), With<Bonfire>>,
) {
    if let Ok((mut wood, mut sprite)) = query.single_mut() {
        wood.quantity.sub(0.5 * time.delta_seconds());

        if wood.quantity.value() >= 66.0 {
            sprite.index = 3;
        } else if wood.quantity.value() >= 33.0 {
            sprite.index = 2;
        } else if wood.quantity.value() > 0.0 {
            sprite.index = 1;
        } else {
            sprite.index = 0;
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn player_interaction(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    keyboard_input: Res<Input<KeyCode>>,
    mut query_set: QuerySet<(
        Query<(&Transform, &Direction, &mut WoodInventory), WithAlivePlayer>,
        Query<&mut BonfireWood, With<Bonfire>>,
    )>,
) {
    if keyboard_input.pressed(KeyCode::Space) {
        let bonfire_position = Vec3::new(0.0, 0.0, 1.0);
        let bonfire_size = Vec2::new(16.0, 16.0);

        let mut player_position = Vec3::new(0.0, 0.0, 0.0);
        let player_size = Vec2::new(16.0, 16.0);

        let mut wood_in_inventory: f32 = 0.0;
        let mut leftover_wood: f32 = 0.0;

        let mut collision_detected = false;

        if let Ok((player_transform, direction, wood)) = query_set.q0_mut().single_mut() {
            match direction.kind {
                DirectionKind::Up => {
                    player_position.y += 4.0;
                }
                DirectionKind::Down => {
                    player_position.y -= 4.0;
                }
                DirectionKind::Left => {
                    player_position.x -= 4.0;
                }
                DirectionKind::Right => {
                    player_position.x += 4.0;
                }
                _ => {}
            }

            player_position += player_transform.translation;

            wood_in_inventory = wood.quantity.value();
        }

        if let Ok(mut wood) = query_set.q1_mut().single_mut() {
            collision_detected =
                is_colliding(bonfire_position, bonfire_size, player_position, player_size);

            if collision_detected {
                let needed_wood = 100.0 - wood.quantity.value();

                if wood_in_inventory > needed_wood {
                    wood.quantity.add(needed_wood);
                    leftover_wood = wood_in_inventory - needed_wood;
                } else {
                    wood.quantity.add(wood_in_inventory);
                    leftover_wood = 0.0;
                }
            }
        }

        if let Ok((_, _, mut wood)) = query_set.q0_mut().single_mut() {
            if collision_detected {
                let total_wood = wood.quantity.value();
                wood.quantity.sub(total_wood);
                wood.quantity.add(leftover_wood);

                audio.stop();
                audio.play(asset_server.load("audio/bonfire.mp3"));
            }
        }
    }
}

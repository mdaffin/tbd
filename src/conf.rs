use bevy::{prelude::*, window::WindowMode};

pub struct Config;
// TODO: move to config to ron file and Config struct
pub const GAME_TITLE: &str = "TBD";
pub const GAME_WIDTH: f32 = 256.0;
pub const GAME_HEIGHT: f32 = 144.0;
pub const GAME_SCALE: f32 = 4.0;

const WINDOW_RESIZABLE: bool = true;
const WINDOW_MODE: WindowMode = WindowMode::BorderlessFullscreen;

pub const TILE_SIZE: f32 = 16.0;
pub const SCREEN_WIDTH_IN_TILES: f32 = 16.0;
pub const SCREEN_HEIGHT_IN_TILES: f32 = 9.0;

pub const PLAYER_VELOCITY: f32 = 56.0;

impl Plugin for Config {
    fn build(&self, app: &mut AppBuilder) {
        app.insert_resource(WindowDescriptor {
            title: GAME_TITLE.to_string(),
            width: GAME_WIDTH * GAME_SCALE,
            height: GAME_HEIGHT * GAME_SCALE,
            vsync: false,
            resizable: WINDOW_RESIZABLE,
            mode: WINDOW_MODE,
            ..Default::default()
        });
    }
}

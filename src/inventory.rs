use bevy::prelude::*;

use crate::components::generic::CappedValue;

pub struct FoodInventory {
    pub quantity: CappedValue,
}

pub struct WoodInventory {
    pub quantity: CappedValue,
}

#[derive(Bundle)]
pub struct InventoryBundle {
    pub food: FoodInventory,
    pub wood: WoodInventory,
}
